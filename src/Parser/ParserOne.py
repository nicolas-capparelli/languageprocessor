#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

from src.Lexer.Scanner import Scanner
from StatementDefinitions import *
from src.Parser.Statement import Statement


"""
    First iteration of parser, deprecated for ParserTwo
"""


class Parser:

    def __init__(self, filepath):
        self.filepath = filepath
        self.scanner = Scanner(self.filepath)
        self.current_token = None
        self.statement_list = []

        self.statement_map = {
            IMPORT: StatementRule("IMPORT", IMPORT_RULES),
            FUNCTION: StatementRule("FUNCTION_SIG", FUNCTION_SIG_RULES),
            ENDFUN: StatementRule("ENDFUN", ENDFUN_RULES),
            IDENTIFIER: StatementRule("VARIABLE", IDENTIFIER_RULES)
        }

    def begin_parsing(self):
        self.request_next_token()
        while not self.scanner.finished:
            statement = self.parse_token()
            if statement is not None:
                self.statement_list.append(statement)
            else:
                self.request_next_token()

        # print(f"symbol_table: {self.scanner.symbol_table}")
        self.print_statement_list()

    def request_next_token(self):
        self.current_token = self.scanner.get_next_token()

    def parse_token(self):
        if self.current_token.token == EOF or self.current_token.token == EOL:
            return None

        statement = TOKEN_STATEMENT_MAP[self.current_token.token]
        statement_tokens = []
        for rule in statement.rules:
            if isinstance(rule, list):
                if self.current_token.token in rule:
                    statement_tokens.append(self.current_token)
                    self.request_next_token()
                else:
                    print(self.current_token)

            elif self.current_token.token == rule:
                statement_tokens.append(self.current_token)
                self.request_next_token()

        return Statement(statement.type, statement_tokens)

    def print_statement_list(self):
        print("[")
        for statement in self.statement_list:
            print(statement)
        print("]")


