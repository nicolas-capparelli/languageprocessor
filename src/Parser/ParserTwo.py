#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

from src.Lexer.LexUnit import LexUnit
from src.Lexer.Scanner import Scanner
from StatementDefinitions import *
from src.Parser.Statement import Statement


# Custom exception thrown by ParserTwo when it finds an error in the input it's parsing
class ParserSyntaxError(Exception):
    def __init__(self, token: LexUnit, error_msg):
        self.token = token
        self.error_msg = error_msg

    def __repr__(self):
        return f'Syntax error at line {self.token.line_num} row {self.token.col_num}: {self.error_msg}'


# Final iteration of parser for Deliverable 2
class ParserTwo:

    def __init__(self, filepath):
        self.filepath = filepath
        self.scanner = Scanner(self.filepath)
        self.current_token = None
        self.statement_list = []

        self.statement_map = {
            IMPORT: self.parse_import,
            FUNCTION: self.parse_function_sig,
            ENDFUN: self.parse_endfun,
            IDENTIFIER: self.parse_identifier,
            RETURN: self.parse_return,
            DISPLAY: self.parse_display,
            MAIN: self.parse_main,
            FUNCTION_IDENTIFIER: self.parse_function_call,
            EXIT: self.parse_exit
        }

    def begin_parsing(self):
        """
            Requests tokens from Scanner one at a time
            Runs specific parsing functions using self.statement_map
            Adds returned statement from said parsing function to self.statement_list
        """

        self.request_next_token()
        while not self.scanner.finished:
            if self.current_token.token != EOL and self.current_token.token != EOF:
                statement = self.statement_map[self.current_token.token]()
                self.statement_list.append(statement)
            else:
                self.request_next_token()

    def request_next_token(self):
        self.current_token = self.scanner.get_next_token()

    def parse_import(self):
        statement_tokens = []
        for rule in TOKEN_RULE_MAP[self.current_token.token].rules:
            if self.current_token.token == rule:
                statement_tokens.append(self.current_token)
                self.request_next_token()
            else:
                raise ParserSyntaxError(self.current_token, f'Unexpected token')

        return Statement("IMPORT_STATEMENT", statement_tokens)

    # FUNCTION RELATED
    def parse_function_sig(self):

        statement_tokens = []
        for rule in TOKEN_RULE_MAP[self.current_token.token].rules:

            # Parse function parameters
            if self.current_token.token == PARAMS:
                statement_tokens.append(self.current_token)
                tokens = self.parse_function_params()
                statement_tokens += tokens

            elif self.current_token.token == rule:
                statement_tokens.append(self.current_token)
                self.request_next_token()
            else:
                raise ParserSyntaxError(self.current_token, f'Unexpected token')

        return Statement("FUNCTION_SIG_STATEMENT", statement_tokens)

    def parse_function_params(self):
        """
        Parses all function parameters by checking that all tokens follow the grammar for parameters:
        [IDENTIFIER, COMMA_OP] until the SIGNATURE_TERMINATOR or R_PAREN token is reached, which denotes the end of the
        parameters
        """

        self.request_next_token()
        statement_tokens = []

        # Alternate between checking for IDENTIFIER and COMMA_OP to parse multiple parameters
        flip_flop = 0
        while self.current_token.token != SIGNATURE_TERMINATOR and self.current_token.token != R_PAREN:

            if self.current_token.token == PARAM_RULES[flip_flop]:
                statement_tokens.append(self.current_token)
                self.request_next_token()
            else:
                raise ParserSyntaxError(self.current_token, f"Unexpected value: {self.current_token.lexeme}")

            flip_flop += 1
            flip_flop = flip_flop % 2
        statement_tokens.append(self.current_token)
        self.request_next_token()
        return statement_tokens

    def parse_return(self):
        statement_tokens = [self.current_token]

        self.request_next_token()
        if self.current_token.token == IDENTIFIER:
            statement_tokens.append(self.current_token)
            self.request_next_token()
            return Statement("RETURN_STATEMENT", statement_tokens)
        else:
            raise ParserSyntaxError(self.current_token, f'Unexpected token')

    def parse_endfun(self):
        statement_tokens = [self.current_token]

        self.request_next_token()
        if self.current_token.token == FUNCTION_IDENTIFIER or self.current_token.token == MAIN:
            statement_tokens.append(self.current_token)
            self.request_next_token()
            return Statement("ENDFUN_STATEMENT", statement_tokens)
        else:
            raise ParserSyntaxError(self.current_token, f'Unexpected token')

    def parse_function_call(self):

        statement_tokens = [self.current_token]

        self.request_next_token()
        l_paren = self.current_token

        self.request_next_token()
        next_token = self.current_token

        # Parse function call with no parameters
        if l_paren.token == L_PAREN and next_token.token == R_PAREN:
            statement_tokens.append(l_paren)
            statement_tokens.append(next_token)
            self.request_next_token()
            return Statement("FUNCTION_CALL_STATEMENT", statement_tokens)

        # Parse function call with parameters
        elif l_paren.token == L_PAREN and (next_token.token == IDENTIFIER or
                                           next_token.token in [STRING_LITERAL, DOUBLE_LITERAL, INT_LITERAL]):
            statement_tokens.append(l_paren)
            statement_tokens.append(next_token)

            # Account for the leading COMMA_OP
            self.request_next_token()
            statement_tokens.append(self.current_token)

            tokens = self.parse_function_params()
            statement_tokens += tokens
            return Statement("FUNCTION_CALL_STATEMENT", statement_tokens)

        else:
            raise ParserSyntaxError(self.current_token, f'Expected (), got {self.current_token.lexeme}')

    # IDENTIFIER RELATED
    def parse_identifier(self):

        statement_tokens = []
        for rule in TOKEN_RULE_MAP[self.current_token.token].rules:

            if self.current_token.token == ASSIGN_OP:
                statement_tokens.append(self.current_token)
                tokens = self.parse_expression()
                statement_tokens += tokens

            elif self.current_token.token == rule:
                statement_tokens.append(self.current_token)
                self.request_next_token()
            else:
                raise ParserSyntaxError(self.current_token, f'Unexpected token')
        return Statement("ASSIGNMENT_STATEMENT", statement_tokens)

    def parse_expression(self):
        self.request_next_token()
        statement_tokens = []

        flip_flop = 0
        while self.current_token.token != EOL:

            # Handle RHS input prompt
            if self.current_token.token == INPUT:
                statement_tokens.append(self.current_token)
                self.request_next_token()
                if self.current_token.token == STRING_LITERAL:
                    statement_tokens.append(self.current_token)
                    self.request_next_token()
                else:
                    raise ParserSyntaxError(self.current_token, f'Expected string, got {self.current_token.lexeme}')

            # Handle RHS function call
            elif self.current_token.token == FUNCTION_IDENTIFIER:
                statement_tokens = self.parse_function_call().token_list

            # Handle all other RHS cases
            elif self.current_token.token in EXPR_RULES[flip_flop]:
                statement_tokens.append(self.current_token)
                self.request_next_token()
            else:
                raise ParserSyntaxError(self.current_token, f'Expected {EXPR_RULES[flip_flop]}, got {self.current_token.token}')

            flip_flop += 1
            flip_flop = flip_flop % 2

        return statement_tokens

    # OTHER PARSING FUNCTIONS
    def parse_main(self):
        statement_tokens = [self.current_token]
        self.request_next_token()
        return Statement("MAIN_DEF_STATEMENT", statement_tokens)

    def parse_exit(self):
        statement_tokens = [self.current_token]
        self.request_next_token()

        if self.current_token.token == INT_LITERAL:
            statement_tokens.append(self.current_token)
            self.request_next_token()
            return Statement("EXIT_STATEMENT", statement_tokens)
        else:
            raise ParserSyntaxError(self.current_token, f'Expected int, got {self.current_token.lexeme}')

    def parse_display(self):
        statement_tokens = []
        for rule in TOKEN_RULE_MAP[self.current_token.token].rules:
            if self.current_token.token == rule:
                statement_tokens.append(self.current_token)
                self.request_next_token()
            else:
                raise ParserSyntaxError(self.current_token, f'Unexpected token')

        return Statement("DISPLAY_STATEMENT", statement_tokens)

    # MISC FUNCTIONS
    def print_statement_list(self):
        for statement in self.statement_list:
            print(statement)
            print()
