#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

import sys
from src.Parser.ParserTwo import ParserTwo


def main():
    file = sys.argv[1]
    parser = ParserTwo(file)
    parser.begin_parsing()
    parser.print_statement_list()


if __name__ == '__main__':
    main()
