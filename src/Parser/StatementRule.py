#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

class StatementRule:
    def __init__(self, statement_type, rules):
        self.type = statement_type
        self.rules = rules
