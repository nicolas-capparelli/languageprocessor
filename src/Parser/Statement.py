#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

class Statement:
    def __init__(self, statement_type, token_list):
        self.type = statement_type
        self.token_list = token_list

    def __str__(self):
        return f'{self.type}: {self.token_list}'

    def __repr__(self):
        return f'{self.type}: {self.token_list}'
