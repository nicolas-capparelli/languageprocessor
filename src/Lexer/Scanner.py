#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

from src.Lexer.LexUnit import LexUnit
from src.Lexer.definitions import *
from src.Lexer.operators import OPERATORS

KEYWORDS = {
    "import": IMPORT,
    "implementations": IMPLEMENTATIONS,
    "function": FUNCTION,
    "double": DOUBLE,
    "display": DISPLAY,
    "endfun": ENDFUN,
    "description": DESCRIPTION,
    "return": RETURN,
    "exit": EXIT,
    "input": INPUT,
    "if": IF,
    "else": ELSE,
    "endif": ENDIF,
    "main": MAIN,
    "params": PARAMS
}

""" This scanner is based off of the third approach mentioned in the book, """


class Scanner:
    prev_char = ''
    cur_char = ''
    next_char = ''

    cur_index = 0
    line_num = 1
    col_num = 1

    lookups = []
    finished = False

    last_lexeme = None
    token_buffer = []
    symbol_table = {}

    errors = []

    def __init__(self, filepath):
        self.filepath = filepath
        f = open(self.filepath, "r")
        self.source_string = "".join(f.readlines())
        f.close()

        self.cur_char = self.source_string[self.cur_index]
        self.next_char = self.source_string[self.cur_index + 1]
        self.file_char_count = len(self.source_string)

    def get_next_token(self):
        if self.cur_char is None and self.next_char is None:
            self.finished = True
            return LexUnit(EOF, "", self.line_num, self.col_num)
        token = self.map_char_to_fun()
        if type(token) is LexUnit:
            self.last_lexeme = token.lexeme
            self.forward()
            return token
        else:
            self.forward()
            return token

    def map_char_to_fun(self):

        if '\n' in self.cur_char:
            line = self.line_num
            col = self.col_num

            # Because it's a line break, line_num needs to be incremented and col_num needs to reset
            self.line_num += 1
            self.col_num = 1
            return LexUnit(EOL, "\\n", line, col)

        while self.cur_char.isspace() or "\t" in self.cur_char or self.cur_char == '':
            self.forward()

        if (self.cur_char == '/' and self.next_char == '/') or self.last_lexeme == "description":
            return self.comment_check()

        if self.cur_char in OPERATORS:
            return self.operator_lookup()

        if self.cur_char.isdigit():
            return self.number_literal_lookup()

        if self.cur_char == '"':
            return self.string_literal_lookup()

        if self.cur_char.isalnum():
            return self.word_lookup()

        self.errors.append({'line': self.line_num, "col": self.col_num})
        return self.errors

    def forward(self):
        self.cur_index += 1
        self.col_num += 1

        if self.next_char is None:
            self.cur_char = None
            return

        if self.cur_char is None:
            self.finished = True
            return

        if self.cur_index + 1 >= self.file_char_count - 1:
            self.cur_char = self.next_char
            self.next_char = None
            return

        self.cur_char = self.next_char
        self.next_char = self.source_string[self.cur_index + 1]

    def end_line_check(self):
        if '\n' in self.cur_char:
            while self.cur_char == '\n':
                self.line_num += 1
                self.col_num = 1
                self.forward()

    def comment_check(self):
        lexeme = self.cur_char
        if self.last_lexeme == "description":
            while (self.cur_char != '*' and self.next_char != "/") and self.cur_char is not None:
                self.forward()
                lexeme += self.cur_char
                if "\n" in self.cur_char:
                    self.line_num += 1
                    self.col_num = 0
            self.forward()
            lexeme += self.cur_char
            return LexUnit(MULTI_COMMENT, lexeme, self.line_num, self.col_num)

        else:
            while '\n' not in self.next_char and self.cur_char is not None:
                self.forward()
                lexeme += self.cur_char
                if "\n" in self.cur_char:
                    self.line_num += 1
                    self.col_num = 0
            return LexUnit(COMMENT, lexeme, self.line_num, self.col_num)

    def operator_lookup(self):
        combined_operator = self.cur_char + self.next_char
        if combined_operator in OPERATORS:
            return LexUnit(OPERATORS[combined_operator], combined_operator, self.line_num, self.col_num)
        else:
            return LexUnit(OPERATORS[self.cur_char], self.cur_char, self.line_num, self.col_num)

    def number_literal_lookup(self):
        lexeme = self.cur_char
        while self.next_char.isdigit() or self.next_char == '.':
            self.forward()
            lexeme += self.cur_char
        if "." in lexeme:
            return LexUnit(DOUBLE_LITERAL, lexeme, self.line_num, self.col_num)
        else:
            return LexUnit(INT_LITERAL, lexeme, self.line_num, self.col_num)

    def string_literal_lookup(self):
        lexeme = self.cur_char
        while self.next_char != '"' and self.cur_char is not None:
            self.forward()
            lexeme += self.cur_char
        self.forward()
        lexeme += self.cur_char
        return LexUnit(STRING_LITERAL, lexeme, self.line_num, self.col_num)

    def word_lookup(self):
        lexeme = self.cur_char
        while self.next_char is not None and (self.next_char.isalnum() or self.next_char == "_"):
            self.forward()
            lexeme += self.cur_char
            if lexeme in KEYWORDS:
                return LexUnit(KEYWORDS[lexeme], lexeme, self.line_num, self.col_num)

        # Add identifier to symbol table
        self.symbol_table[lexeme] = None
        if self.last_lexeme == "function" or self.next_char == "(" or self.last_lexeme == "endfun":
            return LexUnit(FUNCTION_IDENTIFIER, lexeme, self.line_num, self.col_num)
        else:
            return LexUnit(IDENTIFIER, lexeme, self.line_num, self.col_num)

