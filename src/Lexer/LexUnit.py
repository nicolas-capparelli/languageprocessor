#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

class LexUnit:
    token = None
    lexeme = None
    line_num = None
    col_num = None

    def __init__(self, token, lexeme, line_num, col_num):
        self.token = token
        self.lexeme = lexeme
        self.line_num = line_num
        self.col_num = col_num

    def __str__(self):
        return f"LexUnit( {self.token}, '{self.lexeme}' ) at line {self.line_num} col {self.col_num}"

    def __repr__(self):
        return f'<{self.token}: {self.lexeme}>'
