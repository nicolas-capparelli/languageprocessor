#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

from Scanner import Scanner


def test_imports():
    scanner = Scanner('../../Files/parse.scl')
    while not scanner.finished:
        token = scanner.get_next_token()
        print(token)


if __name__ == '__main__':
    test_imports()