#  Nicolas Capparelli, Nisha Kakri, Hudson Topping
#  CS4308 – Concepts of Programming Language
#  Spring 2021

from src.Lexer.definitions import *

OPERATORS = {
    "+": ADD_OP,
    "-": SUB_OP,
    "*": MULT_OP,
    "/": DIV_OP,
    "=": ASSIGN_OP,
    "==": EQUALITY_OP,
    "<": LESS_THAN_OP,
    "<=": LESS_THAN_E_OP,
    ">": GREATER_THAN_OP,
    ">=": GREATER_THAN_E_OP,
    ",": COMMA_OP,
    ":": SIGNATURE_TERMINATOR,
    "(": L_PAREN,
    ")": R_PAREN
}
